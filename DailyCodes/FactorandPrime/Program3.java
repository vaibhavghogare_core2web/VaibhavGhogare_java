//Prime Number or not
import java.util.*;

class Program{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Num : ");
		int num = sc.nextInt();

		int temp=1;
		int count=0;

		while(temp<=num){
			
			if(temp%num==0){
				count++;
			}
			temp++;
		}
		if(count==2){
			System.out.print(num+" is a Prime Number");
		}
		else{
			System.out.print(num+" is not a Prime Number");
		}
	}
}
