import java.util.*;

class Program{
	public static void main(String[] args){

		Scanner sc = new Scanner (System.in);

		System.out.print("Enter Num : ");
		int num = sc.nextInt();

		int fact=1;
		int temp=num;
		while(num>=1){

			fact=num*fact;
			num--;
		}
		System.out.println("Factorial of "+temp+" is "+fact);
	}
}

