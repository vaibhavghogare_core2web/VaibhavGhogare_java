//Factorial of number
import java.util.*;

class Program{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Num : ");
		int num = sc.nextInt();

		System.out.print(" ");

		int fact=1;
		while(num>=1){

			fact=num*fact;
		        num--;
		}
		System.out.print(fact+" ");
	}
}
	
		
