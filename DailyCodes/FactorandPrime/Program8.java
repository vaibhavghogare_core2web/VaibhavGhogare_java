import java.util.*;

class Program{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Num : ");
		int num = sc.nextInt();

		int rem=0;
		int rev=0;
		int temp=num;
		while(num>0){

			rem=num%10;
			rev=rev*10+rem;
			num/=10;
		}
		System.out.print(rev+" ");
	}
}
