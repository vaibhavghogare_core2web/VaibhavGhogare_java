//Program to print Pattern
//  A  B  C  D
//  a  b  c  d
//  A  B  C  D
//  a  b  c  d


class Demo{
	public static void main(String[] args){

		for(int i=1;i<=2;i++){

			char ch1='A';
			char ch2='a';

			for(int j=1;j<=4;j++){

				System.out.print(ch1++ + " ");
			}
			System.out.println();

			for(int j=1;j<=4;j++){

                                System.out.print(ch2++ + " ");
                        }
                        System.out.println();
		}
	}
}

