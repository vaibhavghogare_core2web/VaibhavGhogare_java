//Program to print pattern
//  A  B  C  D
//  a  b  c  d
//  E  F  G  H
//  e  f  g  h


class Demo{
        public static void main(String[] args){

                char ch1='A';
                char ch2='a';

                for(int i=1;i<=2;i++){

                        for(int j=1;j<=4;j++){
                                System.out.print(ch1++ +" ");
                        }
                        System.out.println();

                        for(int j=1;j<=4;j++){
                                System.out.print(ch2++ +" ");
                        }
                        System.out.println();
                }
        }
}
