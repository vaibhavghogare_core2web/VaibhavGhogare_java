/*9.Write a program to print the temperature of the Air Conditioner in degrees and also print the standard room temperature using appropriate data types*/
class AirConditionerTemperature {
    public static void main(String[] args) {
        // Representing temperatures using appropriate data types
        float acTemperature = 22.5f; // Example: 22.5 degrees Celsius
        byte standardRoomTemperature = 25; // Example: 25 degrees Celsius

        // Printing the temperature of the air conditioner and standard room temperature
        System.out.println("Air Conditioner Temperature: " + acTemperature + " degrees Celsius");
        System.out.println("Standard Room Temperature: " + standardRoomTemperature + " degrees Celsius");
    }
}

