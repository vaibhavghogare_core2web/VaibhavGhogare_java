/* 6. A person is storing date,month and year in variable(Write a code to print the date,month,year and also print the total seconds in a day,months and year.*/
class DateDetails {
    public static void main(String[] args) {
        // Storing date, month, and year in variables
        byte date = 1;
        String month = "January";
        short year = 2024;

        // Printing date, month, and year
        System.out.println("Date: " + date);
        System.out.println("Month: " + month);
        System.out.println("Year: " + year);

        // Calculating and printing total seconds in a day, month, and year
        byte secondsInMinute = 60;
        byte minutesInHour = 60;
        byte hoursInDay = 24;

        int secondsInDay = secondsInMinute * minutesInHour * hoursInDay;
        int secondsInMonth = secondsInDay * 30; // Assuming an average month length of 30 days
	int secondsInYear = secondsInDay * 365; // Assuming a non-leap year

        System.out.println("\nTotal Seconds in a Day: " + secondsInDay);
        System.out.println("Total Seconds in a Month: " + secondsInMonth);
        System.out.println("Total Seconds in a Year: " + secondsInYear);
    }
}

