/*11.Write a program to print the value of gravity and the print the letter used to represent the acceleration due to gravity.*/
class GravityDetails {
    public static void main(String[] args) {
        // Representing the value of gravity using appropriate data type
        float gravityValue = 9.81f; // Example: 9.81 m/s^2

        // Representing the letter used to represent acceleration due to gravity
        char gravityLetter = 'g';

        // Printing the value of gravity and the letter
        System.out.println("Value of Gravity: " + gravityValue);
        System.out.println("Letter for Acceleration due to Gravity: " + gravityLetter);
    }
}

