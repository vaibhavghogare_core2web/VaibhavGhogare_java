// 2. A person in the bank and wants to print the bank balance also wants to print the bank address
class BankDetails {
    public static void main(String[] args) {
        // Representing bank balance and address using appropriate data types
        float bankBalance = 5000.75f; // Example balance: $5000.75
        String bankAddress = "123 Main Street, Cityville, Country"; // Example address

        // Printing bank balance and address
        System.out.println("Bank Balance: $" + bankBalance);
        System.out.println("Bank Address: " + bankAddress);
    }
}
