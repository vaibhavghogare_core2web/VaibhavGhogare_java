/* 3.A scientist is working on his research and wants to find an area using pi.(Write a code to print the value of pi angle of triangle).Scientists are also eager to print the highest degree of angle of circle(360 degree).*/
class ScientistWork {
    public static void main(String[] args) {
        // Printing the value of pi
        double pi = Math.PI;
        System.out.println("Value of pi: " + pi);

        // Printing the highest degree of an angle in a circle
        int circleDegrees = 360;
        System.out.println("Highest degree of angle in a circle: " + circleDegrees);
    }
}
