// 1.A student in class 11th and there are 12 devision of the class which data type is feasible to print the class and divisions
class StudentDetails {
    public static void main(String[] args) {
        // Representing class and division using appropriate data types
        byte studentClass = 11;
        byte studentDivision = 12; // Example: 11th class, 2nd division

        // Printing class and division
        System.out.println("Class: " + studentClass);
        System.out.println("Division: " + studentDivision);
    }
}

