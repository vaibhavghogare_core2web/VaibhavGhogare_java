/* 13.Write a program to print the current and voltage.*/
class ElectricalDetails {
    public static void main(String[] args) {
        // Representing current and voltage using appropriate data types
        float current = 5.0f; // Example: 5.0 Amperes
        float voltage = 220.0f; // Example: 220.0 Volts

        // Printing the current and voltage
        System.out.println("Current: " + current + " Amperes");
        System.out.println("Voltage: " + voltage + " Volts");
    }
}
