/* 12.Write a program to print the quantity of liters and weight in grams*/
class QuantityDetails {
    public static void main(String[] args) {
        // Representing quantity of liters and weight in grams using appropriate data types
        float litersQuantity = 2.5f; // Example: 2.5 liters
        float weightInGrams = 1500.0f; // Example: 1500 grams

        // Printing the quantity of liters and weight in grams
        System.out.println("Quantity of Liters: " + litersQuantity + " liters");
        System.out.println("Weight in Grams: " + weightInGrams + " grams");
    }
}

