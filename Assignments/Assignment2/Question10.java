/*10. Write a program to print the box office collection and imdb rating of a movie.*/
class MovieDetails {
    public static void main(String[] args) {
        // Representing box office collection and IMDb rating using appropriate data types
        long boxOfficeCollection = 2545316564L; // Example: $150 million
        float imdbRating = 8.5f; // Example: IMDb rating of 8.5

        // Printing box office collection and IMDb rating
        System.out.println("Box Office Collection: RS" + boxOfficeCollection);
        System.out.println("IMDb Rating: " + imdbRating);
    }
}

