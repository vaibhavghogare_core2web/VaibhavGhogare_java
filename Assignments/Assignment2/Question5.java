/*5.A person is calculating the count of a button on a keyboard(write a code to print the count of buttons and also wants to print the cost of his keyboard).*/
class KeyboardDetails {
    public static void main(String[] args) {
        // Representing the count of buttons and cost using appropriate data types
        byte buttonCount = 104; // Example: 104 buttons
        short keyboardCost = 15000; // Example cost: $49.99

        // Printing the count of buttons and the cost of the keyboard
        System.out.println("Button Count: " + buttonCount);
        System.out.println("Keyboard Cost: Rs " + keyboardCost);
    }
}

