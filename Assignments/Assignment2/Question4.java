/* 4.An organization is taking surveys of companies in which it is taking Employee count of company.(write a code to print the employee count of the company and total expenses of the company on travel)*/
class CompanySurvey {
    public static void main(String[] args) {
        // Representing employee count and total expenses using appropriate data types
        byte employeeCount = 100; // Example: 100 employees
        int totalTravelExpenses = 50000; // Example expenses: $50000.50

        // Printing employee count and total travel expenses
        System.out.println("Employee Count: " + employeeCount);
        System.out.println("Total Travel Expenses: Rs" + totalTravelExpenses);
    }
}

