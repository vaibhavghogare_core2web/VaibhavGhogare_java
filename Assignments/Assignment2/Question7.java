/* 7.A person is trying to save the world data he wants to store the world's population and also wants to store India's population(Print the world's and India's population using appropriate data types).*/
class WorldPopulation {
    public static void main(String[] args) {
        // Representing world population and India's population using appropriate data types
        long worldPopulation = 8019876189L; // Example: 7.8 billion
        int  indiaPopulation = 1441719852; // Example: 1.393 billion

        // Printing world population and India's population
        System.out.println("World Population: " + worldPopulation);
        System.out.println("India Population: " + indiaPopulation);
    }
}

