class Program{
        public static void main(String[] args){

                int num=256985;
		int sum=0;

                System.out.print("Sum of Digits : ");

                while(num>0){

                        int rem=num%10;
                        num=num/10;

			if(rem%2==0){
				sum=sum+rem;
			}

		}
		System.out.print(sum);
		System.out.println();
	
	}
}
