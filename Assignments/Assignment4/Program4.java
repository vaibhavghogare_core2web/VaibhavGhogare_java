class Program{
        public static void main(String[] args){

                int num=256985;

                System.out.print("Square of odd numbers : ");

                while(num>0){

                        int rem=num%10;
                        num=num/10;
			
			if(rem%2==1){

				System.out.print(rem*rem+" ");
			}
		}
	}
}
