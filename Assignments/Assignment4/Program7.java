class Program{
        public static void main(String[] args){

                int num=256985;
                int product=1;

                System.out.print("Sum of Digits : ");

                while(num>0){

                        int rem=num%10;
                        num=num/10;

			if(rem%2!=0){
				product*=rem;
			}
		}
		System.out.print(product);
		System.out.println();
	}
}
