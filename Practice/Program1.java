 import java.util.*;

 class Practice{
	 public static void main(String[] args){

		 Scanner sc = new Scanner(System.in);

		 System.out.print("Enter Number : ");
		 int num = sc.nextInt();

		 int temp=1;
		 int count=0;
		 while(temp<=num){

			 if(num%temp==0){
				 count++;
			 }
			 temp++;
		 }
		 if(count==1){
			 System.out.println(num+" is neither Prime or nor Composite Number");
		 }
		 else if(count==2){
			 System.out.println(num+" is a Prime Number ");
		 }
		 else{
			 System.out.println(num+" is a Composite Number");
		 }
	 }
 }

