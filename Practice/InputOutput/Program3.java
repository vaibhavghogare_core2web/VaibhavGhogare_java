import java.util.*;

class Aarya{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter Size of Array : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Elements : ");
		for(int i=0;i<arr.length;i++){
			arr[i] = sc.nextInt();
		}

		System.out.println("Array Elements are : ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
}
