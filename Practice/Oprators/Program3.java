import java.util.*;

class Practice{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Number1 : ");
		int num1 = sc.nextInt();

		System.out.print("Enter Number2 : ");
		int num2 = sc.nextInt();

		System.out.println("Number 1 is Greater than Number 2 :"+(num1>num2));
		System.out.println("Number 1 is Smaller than Number 2 :"+(num1<num2));
		System.out.println("Number 1 is Equal to Number 2 :"+(num1==num2));
		System.out.println("Number 1 is not Equal than Number 2 :"+(num1!=num2));
		System.out.println("Number 1 is Greater than Equal to Number 2 :"+(num1>=num2));
		System.out.println("Number 1 is Smaller than Equal to Number 2 :"+(num1<=num2));
	}
}
