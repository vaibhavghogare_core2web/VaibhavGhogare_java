// 2DArray

import java.util.*;

class Demo{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Row : ");
		int row = sc.nextInt();

		System.out.print("Enter Column : ");
		int column = sc.nextInt();

		int arr[][] = new int[row][column];

		System.out.println("Enter array elements : ");
		for(int i=0;i<row;i++){
			for(int j=0;j<column;j++){
				arr[i][j] = sc.nextInt();
			}
			//System.out.println();
		}

		System.out.println("Array elements are : ");
		for(int i=0;i<row;i++){
			for(int j=0;j<column;j++){
				System.out.print(arr[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println();
	}
}
		
