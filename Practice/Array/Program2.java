import java.util.*;

class Demo{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Array size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		for(int i=0;i<arr.length;i++){
			System.out.print("Enter array elements : ");
			arr[i] = sc.nextInt();
		}
		System.out.println();

		System.out.println("Array elements are : ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println();

		//sum of array elements
		int sum=0;
		for(int i=0;i<arr.length;i++){
			sum+=arr[i];
		}
		System.out.print("Sum of array elements is "+sum);
		System.out.println();

		//even elements from array
		System.out.println("Even elements from array : ");
		for(int i=0;i<arr.length;i++){
			if(i%2==0){
				System.out.print(arr[i]+" ");
			}
		}
		System.out.println();

		//odd indexed elemets
		System.out.println("Odd indexed elements are : ");
		for(int i=0;i<arr.length;i++){
			if(i%2==1){
				System.out.print(arr[i]+" ");
			}
		}
		System.out.println();

		// array elements which is divisible by 4
		System.out.println("Elements are divisible by 4 : ");
		for(int i=0;i<arr.length;i++){
			if(arr[i]%4==0){
				System.out.print(arr[i]+" ");
			}
		}
		System.out.println();

	}
}
