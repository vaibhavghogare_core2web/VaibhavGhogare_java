 // odd indexed elements from array

import java.util.*;

class Program{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter array size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter array elements : ");
		for(int i=0;i<arr.length;i++){
			arr[i] = sc.nextInt();
		}
		System.out.println();

		System.out.println("array elements are : ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println();

		//System.out.println(" is an odd indexed element");
		for(int i=0;i<arr.length;i++){
			if(i%2==1){
				System.out.println(arr[i]+" is an odd indexed element");
			}
		}

			
	}
}

