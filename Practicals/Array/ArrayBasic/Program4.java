//sum of odd numbers in array

import java.util.*;

class Program{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter array size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		for(int i=0;i<arr.length;i++){
			System.out.print("Enter array elements : ");
			arr[i] = sc.nextInt();
		}
		System.out.println("Array elements are : ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println();

		//System.out.print("Sum of odd elements in array : ");
		int sum=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]%2==1){
				sum+=arr[i];
			}
		}
		System.out.print("Sum of odd elements in array : "+sum);
		System.out.println();
	}
}
				




