import java.util.*;

class Program{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter array size : ");
		int size = sc.nextInt();

		int arr[] = new int [size];

		for(int i=0;i<arr.length;i++){
			System.out.print("Enter Array elements : ");
			arr[i] = sc.nextInt();
		}
		System.out.println("Array elements are : ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println();

		for(int i=0;i<arr.length;i++){
			if(arr[i]<10){
				System.out.println(arr[i]+" is less than 10");
			}
		}
		System.out.println();
	}
}
