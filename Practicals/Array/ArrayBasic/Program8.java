
import java.util.*;

class Program{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter count of employee : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter employee age : ");
		for(int i=0;i<arr.length;i++){
			arr[i] = sc.nextInt();
		}
		System.out.println();

		System.out.println("Employee age are given : ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
}
