import java.util.*;

class ArrayPractical{
        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter Size of Array : ");
                int size = sc.nextInt();
		
		char arr[] = new char[size];
		
		System.out.println("Enter Array Elements : ");
		for(int i=0;i<arr.length;i++){
			arr[i] = sc.next().charAt(0);
		}

		System.out.println("Vowel in character array : ");
		for(int i=0;i<arr.length;i++){
			if(arr[i]=='a' || arr[i]=='e' || arr[i]=='i' || arr[i]=='o' || arr[i]=='u' || arr[i]=='A' || arr[i]=='E' || arr[i]=='I' || arr[i]=='O' || arr[i]=='U'){
				System.out.println("Vowel "+ arr[i]+" found at index "+i);
			}
		}
		System.out.println();
	}
}

