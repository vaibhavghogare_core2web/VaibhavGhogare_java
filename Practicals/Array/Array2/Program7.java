import java.util.*;

class ArrayPractical{
        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter Size of Array : ");
                int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Elements : ");
		for(int i=0;i<arr.length;i++){
			arr[i] = sc.nextInt();
		}

		if(size%2==0){
			System.out.print("Array elements are : ");
			//for(int i=0;i<arr.length;i+=2){
			for(int i=0;i<arr.length;i++){
				if(i%2==0){
					System.out.print(arr[i]+" ");
				}
			}
			System.out.println();
		}
		else{
			System.out.print("Array Elements are : ");
			for(int i=0;i<arr.length;i++){
				System.out.print(arr[i]+" ");
			}
			System.out.println();
		}
	}
}

				
