import java.util.*;

class ArrayPractical{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Elements : ");
		for(int i=0;i<arr.length;i++){
			arr[i] = sc.nextInt();
		}

		int count=0;
		System.out.print("Even Elements : ");
		for(int i=0;i<arr.length;i++){
			if(arr[i]%2==0){
				System.out.print(arr[i]+" ");
				count++;
			}
		}
		System.out.println();
		System.out.print("Count : "+count);
		System.out.println();
	}
}

