import java.util.*;

class ArrayPractical{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Array Size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Elements : ");
		for(int i=0;i<arr.length;i++){
			arr[i] = sc.nextInt();
		}

		int pos=0;
		int max=arr[0];
		for(int i=0;i<arr.length;i++){
			if(arr[i]>max){
				max=arr[i];
				pos=i;
			}
		}
		System.out.println("Maximum number in the array is found at position "+pos+" is "+max);
	}
}


