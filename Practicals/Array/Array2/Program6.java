import java.util.*;

class ArrayPractical{
        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter Size of Array : ");
                int size = sc.nextInt();

		int arr[] = new int[size];
		
		System.out.println("Enter Array Elements : ");
		for(int i=0;i<arr.length;i++){
			arr[i] = sc.nextInt();
		}

		int product=1;
		for(int i=0;i<arr.length;i++){
			if(i%2==1){
				product*=arr[i];
			}
		}
		System.out.print("Product of odd indexed elements : "+product);
		System.out.println();
	}
}
		
