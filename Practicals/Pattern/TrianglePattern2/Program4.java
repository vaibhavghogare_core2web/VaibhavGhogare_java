import java.io.*;

class Pattern{
        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter Row : ");
                int row = Integer.parseInt(br.readLine());

                for(int i=1;i<=row;i++){

			int ch=64+row;

			for(int j=1;j<=i;j++){

				if(i%2==0){
					
					System.out.print((char)ch+" ");
					ch--;
				}
				else{
					ch+=32;
					System.out.print((char)ch+" ");
					ch-=33;
				}
	
			}
			System.out.println();
		}
	}
}

