import java.io.*;

class Pattern{
        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter Row : ");
                int row = Integer.parseInt(br.readLine());

                int ch=65;
		for(int i=1;i<=row;i++){
			
			for(int j=1;j<=i;j++){

                                if(i%2==0){

                                        System.out.print((char)ch+" ");
                                }
                                else{
                                        
                                        System.out.print(j+" ");
                                }
				ch++;
                        }
                        System.out.println();
                }
        }
}
