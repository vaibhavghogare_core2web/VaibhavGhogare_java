// 1  a  2  b
// 1  a  2
// 1  a
// 1

import java.util.*;

class Program{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Row : ");
		int row = sc.nextInt();

		for(int i=1;i<=row;i++){
			char ch='a';
			int num=1;

			for(int j=row;j>=i;j--){

				if(j%2==0){

					System.out.print(num+" ");
					num++;
				}
				else{
					System.out.print(ch+" ");
					ch++;
				
				}
		
			}
			System.out.println();
		}
	}
}



