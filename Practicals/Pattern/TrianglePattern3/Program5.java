// A  B  C  D
// a  b  c
// A  B
// a

import java.util.*;

class Program{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter Row : ");
		int row = sc.nextInt();

		for(int i=1;i<=row;i++){
			int ch=65;

			for(int j=row;j>=i;j--){

				if(i%2==0){

					ch+=32;
					System.out.print((char)ch+" ");
					ch-=32;
					ch++;
				}
				else{
					System.out.print((char)ch+" ");
					ch++;
				}
			}
			System.out.println();
		}
	}
}


