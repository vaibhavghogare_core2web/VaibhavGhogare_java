//  4  c  2  a
//  3  b  1
//  2  a
//  1


/*import java.util.*;

class Program{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Row : ");
		int row = sc.nextInt();

		int ch=97;
		for(int i=1;i<=row;i++){

			for(int j=row;j>=i;j--){

				if(j%2==0){

					System.out.print(j+1-i+" ");
				}
				else{
					ch=ch+j-1;
					System.out.print((char)ch+" ");
					ch-=2;
				}
			
			}
			System.out.println();
		}
	}
}*/

import java.util.*;

class Program {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter Row : ");
        int row = sc.nextInt();

        int ch = 97;
        for (int i = 1; i <= row; i++) {

            for (int j = row; j >= i; j--) {

                if (j % 2 == 0) {
                    System.out.print(j + " ");
                } else {
                    System.out.print((char) ch + " ");
                    ch += 2;
                }

            }
            System.out.println();
            ch = 97;
        }
    }
}


