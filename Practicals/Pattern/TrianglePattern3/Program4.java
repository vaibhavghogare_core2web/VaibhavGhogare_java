//  J  I  H  G
//  F  E  D
//  C  B  
//  A
// row = 4

import java .util.*;

class Program{
	public static void main(String[] args){

		Scanner sc = new Scanner (System.in);

		System.out.print("Ente Row : ");
		int row = sc.nextInt();

		System.out.print("Enter ch: ");
		String input = sc.next();
		char ch = input.charAt(0);
		

		for(int i=1;i<=row;i++){

			for(int j=row;j>=i;j--){

				System.out.print(ch+" ");
				ch--;
			}
			System.out.println();
		}
	}
}


