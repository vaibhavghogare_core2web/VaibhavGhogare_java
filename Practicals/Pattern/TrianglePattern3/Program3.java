//  20  18  16 14
//  12  10  8  
//  6  4
//  2
//This program will be writen in two ways are as follow 

// 1 User given number

/*import java.util.*;

class Program{
	public static void main(String[] args){

		Scanner sc = new Scanner (System.in);

		System.out.print("Enter Row : ");
		int row = sc.nextInt();

		System.out.print("Enter Number : ");
		int num = sc.nextInt();

		for(int i=1;i<=row;i++){

			for(int j=row;j>=i;j--){

				System.out.print(num+" ");
				num-=2;
			}
			System.out.println();
		}
	}
}*/

// 2 Number is given from row


import java.util.*;

class Program{
        public static void main(String[] args){

                Scanner sc = new Scanner (System.in);

                System.out.print("Enter Row : ");
                int row = sc.nextInt();

                int num=(row+1)*row;
		for(int i=1;i<=row;i++){

                        for(int j=row;j>=i;j--){

                                System.out.print(num+" ");
                                num-=2;
                        }
                        System.out.println();
                }
        }
}
