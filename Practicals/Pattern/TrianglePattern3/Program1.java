//  1  2  3  4
//  2  3  4
//  3  4  
//  4


import java.util.*;

class Program{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Row : ");
		int row = sc.nextInt();

		for(int i=1;i<=row;i++){

			int num=0;
			num+=i;

			for(int j=row;j>=i;j--){

				System.out.print(num+" ");
				num++;
			}
			System.out.println();
		}
	}
}
