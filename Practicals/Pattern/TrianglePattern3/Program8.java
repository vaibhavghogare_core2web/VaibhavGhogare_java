
import java.util.*;

class Program{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Row : ");
		int row = sc.nextInt();

		int num=row;
		for(int i=1;i<=row;i++){
			int ch=65;

			for(int j=row;j>=i;j--){

				if(i%2==1){
					System.out.print(num+" ");
					num--;
				}
				else{
					
					System.out.print((char)ch+" ");
				}

			}
			num-=2;

			 System.out.println();
		}
	}
}


