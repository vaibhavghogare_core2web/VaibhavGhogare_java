import java.util.*;

class Program{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter row : ");
		int row = sc.nextInt();

		int num=65;

		for(int i=1;i<=row;i++){

			for(int j=row;j>=i;j--){

				if(i%2==0){
					System.out.print((char)(num+row)+" ");
					num--;
				}
				else{
					System.out.print((char)(num+32+row));
					num--;
				}
			}
			System.out.println();
		}
	}
}

