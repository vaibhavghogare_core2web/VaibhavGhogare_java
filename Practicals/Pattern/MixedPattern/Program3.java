 import java.util.*;

 class Pattern{
	 public static void main(String[] args){

		 Scanner sc = new Scanner(System.in);

		 System.out.print("Enter row : ");
		 int row = sc.nextInt();

		 for(int i=1;i<=row;i++){
			 int num=64+row;

			 for(int j=1;j<=row;j++){

				 if(i%2==0){
					 System.out.print(num-64+" ");
					 				 }
				 else{
					 System.out.print((char)num+" ");
				 }
				 num--;
			 }
			 System.out.println();
		 }
	 }
 }
