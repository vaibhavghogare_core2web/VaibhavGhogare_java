import java.util.*;

class Pattern{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Row : ");
		int row = sc.nextInt();

		for(int i=1;i<=row;i++){
			int num = 96+row;
			int num2 = row;
			
			for(int j=1;j<=i;j++){

				if(i%2==1){
					System.out.print((char)num+" ");
					num--;
				}
				else{
					System.out.print(num2+" ");
					num2--;
				}

			}
			System.out.println();
		}
	}
}

