import java.util.*;

class Pattern{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Row : ");
		int row = sc.nextInt();

		int num = row;
		for(int i=1;i<=row;i++){

			for(int j=1;j<=i;j++){

				System.out.print(num*j+" ");

			}
			System.out.println();
			num=row-i;
		}
	}
}

