import java.util.*;

class Pattern{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter row : ");
		int row = sc.nextInt();

		int num =0;
		for(int i=row;i>=1;i--){

			for(int j=i;j>=1;j--){

				System.out.print((num+=2)+" ");
			}
			System.out.println();
		}
	}
}

