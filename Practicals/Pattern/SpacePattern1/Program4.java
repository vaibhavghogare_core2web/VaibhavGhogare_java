
import java.util.*;

class SpacePattern{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Rows : ");
		int row = sc.nextInt();

		int num = 64+row;
		for(int i=1;i<=row;i++){

			for(int space=1;space<=row-i;space++){

				System.out.print("  ");
			}

			for(int j=1;j<=i;j++){
				
				System.out.print((char)num+" ");
				num++;
			}
			num=num-1-i;
			System.out.println();
		}
	}
}

