
import java.util.*;

class SpacePattern{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter Rows : ");
		int row = sc.nextInt();

		for(int i=1;i<=row;i++){
			int ch = 64+i;
			
			for(int sp=1;sp<i;sp++){
				System.out.print("  ");
			}

			for(int j=row;j>=i;j--){
				if((i*j)%2==0){
					System.out.print((char)ch+" ");
				}
				else{
					System.out.print(ch+" ");
				}
				ch++;
			}
			System.out.println();
		}

	}
}
