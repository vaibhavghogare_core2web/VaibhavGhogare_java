import java.util.*;

class SpacePattern{
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows : ");
		int row = sc.nextInt();

		for(int i=1;i<=row;i++){

			for(int space=1;space<i;space++){
				System.out.print("  ");
			}
			int num = 64+row;
			for(int j=row;j>=i;j--){
				System.out.print((char)num+" ");
				num--;
			}
			System.out.println();
		}
	}
}

