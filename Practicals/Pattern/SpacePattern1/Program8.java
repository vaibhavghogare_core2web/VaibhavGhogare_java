import java.util.*;

class SpacePattern{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Rows : ");
		int row = sc.nextInt();

		int num = row;
		for(int i=1;i<=row;i++){

			for(int space=1;space<i;space++){
				System.out.print("  ");
			}
			
			for(int j=row;j>=i;j--){
				System.out.print(num+" ");
			}
			num--;
			System.out.println();
		}
	}
}

			
