import java.util.*;
class MixedPattern{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);

                System.out.print("Enter Rows : ");
                int row = sc.nextInt();

		for(int i=1;i<=row;i++){
			int num=1;

			for(int sp=row;sp>i;sp--){
				System.out.print(" "+" ");
			}
			for(int j=1;j<=i*2-1;j++){
				System.out.print(num+" ");
			num++;	
			}
			System.out.println();
		}
	}
}

