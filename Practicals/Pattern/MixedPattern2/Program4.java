import java.util.*;
class MixedPattern{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);

                System.out.print("Enter Rows : ");
                int row = sc.nextInt();

		for(int i=1;i<=row;i++){
			int num=64+i;

			for(int sp=1;sp<i;sp++){
				System.out.print(" "+" ");
			}
			for(int j=row;j>=i;j--){
				if(row%2==0){
					System.out.print((char)(num+32)+" ");
				}
				else{
					System.out.print((char)num+" ");
				}
				num++;
			}
			System.out.println();
		}
	}
}
			
