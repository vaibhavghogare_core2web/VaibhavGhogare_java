import java.util.*;

class SpacePattern{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Rows : ");
		int row = sc.nextInt();

		int num=1;
		for(int i=1;i<=row;i++){
			//int num=i;

			for(int sp=row;sp>i;sp--){
				System.out.print("  ");
			}

			for(int j=1;j<=i*2-1;j++){
				
				if(i%2==0){
					System.out.print((char)(num+64)+" ");
				}
				else{
					System.out.print(num+" ");
				}
			}
			num++;
			System.out.println();
		}
	}
}

