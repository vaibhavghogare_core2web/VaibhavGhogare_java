import java.util.*;

class SpacePattern{
        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter Rows : ");
                int row = sc.nextInt();

		for(int i=1;i<=row;i++){
			int ch=65;

			//space loop
			for(int sp=row;sp>i;sp--){
				System.out.print("  ");
			}

			for(int j=1;j<=i*2-1;j++){

				if(i%2==0){
					if(j<i){
						System.out.print((char)(ch+32)+" ");  //small letter
						ch++;
					}
					else{
						System.out.print((char)(ch+32)+" ");  //small letter
						ch--;
					}
					
				}
				else{
					if(j<i){
						System.out.print((char)ch+" ");  //capital letter
						ch++;
					}
					else{
						System.out.print((char)ch+" ");  //capital letter
						ch--;
					}

				}
			}
			System.out.println();
		}
	}
}
