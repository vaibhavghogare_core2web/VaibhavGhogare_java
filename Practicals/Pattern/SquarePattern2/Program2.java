import java.util.*;
class SquarePattern{
        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter Rows : ");
                int row = sc.nextInt();

		int num=64+row;
		for(int i=1;i<=row;i++){

			for(int j=row;j>i;j--){
				System.out.print((char)(num+32)+" ");
				num++;
			}
			for(int k=1;k<=i;k++){
				System.out.print((char)num+" ");
				num++;
			}
			System.out.println();
		}
	}
}
