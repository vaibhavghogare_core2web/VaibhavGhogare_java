import java.util.*;
class SquarePattern{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Rows : ");
		int row = sc.nextInt();
		
		int num=64+row;
		for(int i=1;i<=row;i++){

			for(int j=1;j<=row;j++){

				if(j==1){
					System.out.print((char)num+" ");
				}
				else{
					System.out.print((char)(num+32)+" ");
				}
				num++;
			}
			System.out.println();
		}
	}
}

