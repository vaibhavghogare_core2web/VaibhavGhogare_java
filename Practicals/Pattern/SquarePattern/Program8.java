import java.util.*;

class SquarePattern{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Rows : ");
		int row = sc.nextInt();

		for(int i=1;i<=row;i++){
			int num=64+row;

			for(int j=1;j<=row;j++){

				if((i+j)%2==0){
					System.out.print("#"+" ");
				}
				else{
					System.out.print((char)num+" ");
					num--;
				}
			}
			System.out.println();
		}
	}
}
