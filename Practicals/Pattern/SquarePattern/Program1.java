import java.util.*;
class SquarePattern{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Rows : ");
		int row = sc.nextInt();

		for(int i=1;i<=row;i++){
			int num=row;
			if(i%2==0){
				for(int j=1;j<=row;j++){
					System.out.print(num+" ");
				}
			}
			else{
				for(int j=1;j<=row;j++){
					System.out.print((char)(num+64)+" ");
					num--;
				}
				
			}
			System.out.println();
		}
	}
}
