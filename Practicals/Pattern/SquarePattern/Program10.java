import java.util.*;
class SquarePattern{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Rows : ");
		int row = sc.nextInt();

		for(int i=1;i<=row;i++){
		int num=row;

			for(int j=1;j<=row;j++){

				if(i%2==0){
					System.out.print((char)(num+64)+" ");
				}
				else{
					if(j%2==0){
						System.out.print((char)(num+64)+" ");
					}
					else{
						System.out.print(num+" ");
					}
				}
				num--;
			}
			System.out.println();
		}
	}
}

