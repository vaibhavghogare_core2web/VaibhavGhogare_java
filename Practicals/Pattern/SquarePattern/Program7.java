import java.util.*;
class SquarePattern{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Rows : ");
		int row = sc.nextInt();

		int num=row;
		int ch=65;
		for(int i=1;i<=row;i++){

			for(int j=1;j<=row;j++){
				
				if((i+j)%2==0){
					System.out.print((char)ch+" ");
					//System.out.print(num+" ");
				}
				else{
					System.out.print(num+" ");
					//System.out.print((char)ch+" ");
				}
				num++;
			}
			ch++;
			System.out.println();
		}
	}
}
